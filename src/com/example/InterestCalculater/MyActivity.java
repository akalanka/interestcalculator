package com.example.InterestCalculater;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MyActivity extends Activity {
    private static final String EMPTY_STRING = "";

    private EditText amountEditText;
    private EditText rateEditText;
    private TextView yearsTextView;
    private SeekBar yearsSeekBar;
    private TextView interestTextView;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initializeComponents();
    }

    private void initializeComponents() {
        amountEditText = (EditText) findViewById(R.id.amountEditText);
        rateEditText = (EditText) findViewById(R.id.rateEditText);

        yearsSeekBar = (SeekBar) findViewById(R.id.yearsSeekBar);
        yearsSeekBar.setOnSeekBarChangeListener(getYearsSeekBarChangeListener());

        yearsTextView = (TextView) findViewById(R.id.yearsTextView);
        interestTextView = (TextView) findViewById(R.id.interestTextView);

        Button calculateButton = (Button) findViewById(R.id.calculateButton);
        calculateButton.setOnClickListener(getCalculateButtonClickListener());
    }

    private void calculateInterest() {
        Double interest;
        Long amount = null;
        Double rate = null;
        int years = yearsSeekBar.getProgress();

        if (!EMPTY_STRING.equals(amountEditText.getText().toString()) && !EMPTY_STRING.equals(rateEditText.getText().toString())) {
            amount = Long.valueOf(amountEditText.getText().toString());
            rate = Double.valueOf(rateEditText.getText().toString());
        }

        if (amount != null && rate != null && years != 0) {
            interest = amount * (rate / 100) * years;

            String interestLabel = getString(R.string.interest, interest);

            interestTextView.setText(interestLabel);
        }
    }

    private void setNumberOfYears(SeekBar yearsSeekBar) {
        int years = yearsSeekBar.getProgress();

        String yearsLabel = getString(R.string.years, years);

        yearsTextView.setText(yearsLabel);
    }

    /* Event Listeners */
    private View.OnClickListener getCalculateButtonClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateInterest();
            }
        };
    }

    private SeekBar.OnSeekBarChangeListener getYearsSeekBarChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setNumberOfYears(seekBar);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
    }
}
